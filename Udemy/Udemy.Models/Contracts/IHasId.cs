﻿namespace Udemy.Models.Contracts
{
    public interface IHasId
    {
        public int Id { get; set; }
    }
}