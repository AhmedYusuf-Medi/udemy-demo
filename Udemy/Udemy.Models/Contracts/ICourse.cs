﻿namespace Udemy.Models.Contracts
{
    using System.Collections.Generic;
    using Udemy.Models.Models;
    using Udemy.Shared.Enums;

    public interface ICourse : IHasId
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public CourseType Type { get; set; }
        public IEnumerable<Lecture> Lectures { get; }
        public string AddLecture(int id);
        public string RemoveProduct(int id);
    }
}