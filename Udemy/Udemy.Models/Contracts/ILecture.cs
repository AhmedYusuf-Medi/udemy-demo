﻿namespace Udemy.Models.Contracts
{
    public interface ILecture
    {
        public string Name { get; set; }
    }
}