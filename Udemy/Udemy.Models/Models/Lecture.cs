﻿namespace Udemy.Models.Models
{
    using System.Text;
    using Udemy.Models.Contracts;
    using Udemy.Shared.Constants;
    using Udemy.Shared.Extensions;

    public class Lecture : BaseModel, ILecture
    {
        private const string print = "Id: {0}, Lecture name: {1}";
        private string name;

        public Lecture(int id, string name)
            :base(id)
        {
            this.Name = name;
        }

        public string Name 
        { 
            get => this.name;
            set
            {
                value.ThrowIfNullOrWhiteSpace();
                value.ThrowIfOutOfRange(ModelConstraints.Min_Lecture_Name, ModelConstraints.Max_Lecture_Name);

                this.name = value;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(string.Format(print, this.id, this.name));

            return sb.ToString().TrimEnd();
        }
    }
}