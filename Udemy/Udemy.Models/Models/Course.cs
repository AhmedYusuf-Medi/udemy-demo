﻿namespace Udemy.Models.Models
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Udemy.Models.Contracts;
    using Udemy.Shared.Constants;
    using Udemy.Shared.Enums;
    using Udemy.Shared.Extensions;

    public class Course : BaseModel, ICourse
    {
        private string name;
        private decimal price;
        private CourseType courseType;
        private IList<Lecture> lectures;

        public Course(int id, string name, decimal price, CourseType courseType)
            :base(id)
        {
            this.Name = name;
            this.Price = price;
            this.Type = courseType;
            this.lectures = new List<Lecture>();
        }

        public string Name 
        {
            get => this.name;
            set
            {
                value.ThrowIfNullOrWhiteSpace();
                value.ThrowIfOutOfRange(ModelConstraints.Min_Course_Name, ModelConstraints.Max_Course_Name);

                this.name = value;
            } 
        }

        public decimal Price
        {
            get => this.price;
            set
            {
                value.ShouldBeInRange(0, decimal.MaxValue);

                this.price = value;
            }
        }

        public CourseType Type
        {
            get => this.courseType;
            set
            {
                this.courseType = value;
            }
        }

        public IEnumerable<Lecture> Lectures
        {
            get => new List<Lecture>(lectures);
        }

        public string AddLecture(int id)
        {
            Lecture lecture = this.lectures.FirstOrDefault(x => x.Id == id);

            if (lecture != null)
            {
                this.lectures.Add(lecture);

                return string.Format(CommandMessages.Successfully_Added, nameof(Lecture), nameof(Course));
            }

            return string.Format(ExceptionMessages.Already_Registered, lecture.ToString());
        }

        public string RemoveProduct(int id)
        {
            Lecture lecture = this.lectures.FirstOrDefault(x => x.Id == id);

            lecture.ThrowIfNull();

            this.lectures.Remove(lecture);

            return string.Format(CommandMessages.Successfully_Removed, nameof(Lecture), nameof(Course));
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"#Id: {this.id}");
            sb.AppendLine($"#Course: {this.name}");
            sb.AppendLine($"#Course Type: {this.courseType.ToString()}");

            if (this.lectures.Count == 0)
            {
                sb.AppendLine(" #No lectures in this course");
                return sb.ToString().Trim();
            }

            foreach (Lecture lecture in this.lectures)
            {
                sb.AppendLine(lecture.ToString());
                sb.AppendLine(" ===");
            }

            return sb.ToString().TrimEnd();
        }
    }
}