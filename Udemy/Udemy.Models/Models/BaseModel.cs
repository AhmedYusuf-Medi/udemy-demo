﻿namespace Udemy.Models.Models
{
    using Udemy.Models.Contracts;
    using Udemy.Shared.Extensions;

    public abstract class BaseModel : IHasId
    {
        protected int id;

        public BaseModel(int id)
        {
            this.Id = id;
        }

        public int Id
        {
            get => this.id;
            set
            {
                value.ShouldBeInRange(0, int.MaxValue);

                this.id = value;
            }
        }
    }
}