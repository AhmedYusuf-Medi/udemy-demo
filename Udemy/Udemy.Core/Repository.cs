﻿namespace Udemy.Core
{
    using System.Collections.Generic;
    using System.Linq;
    using Udemy.Models.Models;
    using Udemy.Shared.Enums;
    using Udemy.Shared.Extensions;

    public class Repository
    {
        private static int courseId = 0;
        private static int lectureId = 0;

        private readonly List<Course> courses;
        private readonly List<Lecture> lectures;

        public Repository(List<Course> courses, List<Lecture> lectures)
        {
            this.courses = courses.ThrowIfNull();
            this.lectures = lectures.ThrowIfNull();
        }

        public List<Course> Courses => this.courses;

        public List<Lecture> Lectures => this.lectures;

        public void CreateLecture(string name)
        {
            this.lectures.Add(new Lecture(++lectureId, name));
        }

        public void CreateCourse(string name, decimal price, CourseType type)
        {
            this.courses.Add(new Course(++courseId, name, price, type));
        }

        public Course FindCourseByName(string name)
        {
            var course = this.courses.FirstOrDefault(x => x.Name == name);

            course.ThrowIfNull();

            return course;
        }

        public Lecture FindLectureByName(string name)
        {
            var lecture = this.lectures.FirstOrDefault(x => x.Name == name);

            lecture.ThrowIfNull();

            return lecture;
        }

        public Course FindCourseById(int id)
        {
            var course = this.courses.FirstOrDefault(x => x.Id == id);

            course.ThrowIfNull();

            return course;
        }
    }
}