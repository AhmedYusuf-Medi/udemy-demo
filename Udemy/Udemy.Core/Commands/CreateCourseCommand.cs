﻿namespace Udemy.Core.Commands
{
    using System.Collections.Generic;
    using Udemy.Core.Commands.Contracts;
    using Udemy.Core.Contracts;

    public class CreateCourseCommand : BaseCommand
    {
        public CreateCourseCommand(IList<string> commandParameters, IRepository repository) 
            : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            throw new System.NotImplementedException();
        }
    }
}