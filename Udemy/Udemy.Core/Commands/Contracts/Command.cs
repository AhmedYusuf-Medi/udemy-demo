﻿namespace Udemy.Core.Commands.Contracts
{
    using System;
    using System.Collections.Generic;
    using Udemy.Core.Contracts;

    public abstract class BaseCommand : ICommand
    {
        protected BaseCommand(IList<string> commandParameters, IRepository repository)
        {
            this.CommandParameters = commandParameters;
            this.Repository = repository;
        }

        protected BaseCommand(IRepository repository)
        {
            this.Repository = repository;
        }

        public abstract string Execute();

        protected IRepository Repository { get; }

        protected IList<string> CommandParameters { get; }

        protected void ParametersCountValidator(int countMustBe)
        {
            if (CommandParameters.Count != countMustBe)
            {
                throw new ArgumentException($"Invalid number of arguments. Expected: {countMustBe}, Received: {CommandParameters.Count}");
            }
        }
    }
}