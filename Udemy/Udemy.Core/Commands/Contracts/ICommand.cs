﻿namespace Udemy.Core.Commands.Contracts
{
    public interface ICommand
    {
        public string Execute();
    }
}