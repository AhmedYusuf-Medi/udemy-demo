﻿namespace Udemy.Core.IO
{
    public interface IReader
    {
        string CustomReadLine();
    }
}