﻿namespace Udemy.Core.IO
{
    public interface IWriter
    {
        void CustomWriteLine(string text);

        void CustomWrite(string text);
    }
}