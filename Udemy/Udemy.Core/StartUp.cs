﻿namespace Udemy.Core
{
    using CosmeticsShop.Core;
    using System.Collections.Generic;
    using Udemy.Core.IO;
    using Udemy.Models.Models;

    class StartUp
    {
        static void Main(string[] args)
        {
            var repository = new Repository(new List<Course>(), new List<Lecture>());
            var commandFactory = new CommandFactory();
            var engine = new Engine(commandFactory, repository,  new ConsoleWriter(), new ConsoleReader());

            engine.Start();
        }
    }
}