﻿namespace CosmeticsShop.Core
{
    using System;
    using System.Collections.Generic;
    using Udemy.Core;
    using Udemy.Core.Commands.Contracts;
    using Udemy.Core.IO;
    using Udemy.Shared.Extensions;

    public class Engine
    {
        private const string TERMINATION_COMMAND = "end";

        private readonly string[] splitCharacters = { " ", ",", ", ", "|"};
        private readonly CommandFactory commandFactory;
        private readonly Repository productRepository;
        private readonly IWriter writer;
        private readonly IReader reader;

        public Engine(CommandFactory commandFactory, Repository repository, IWriter writer, IReader reader)
        {
            this.commandFactory = commandFactory.ThrowIfNull();
            this.productRepository = repository.ThrowIfNull();
            this.writer = writer.ThrowIfNull();
            this.reader = reader.ThrowIfNull();
        }

        public void Start()
        {
            while (true)
            {
                string commandLine = this.reader.CustomReadLine();
                if (commandLine.ToLower() == TERMINATION_COMMAND)
                {
                    break;
                }

                string output = String.Empty;

                try
                {
                    commandLine.ThrowIfNullOrWhiteSpace();

                    output = this.ProcessCommand(commandLine).Execute();
                }
                catch (Exception exception)
                {
                    output = exception.Message;
                }

                this.writer.CustomWriteLine(output);
            }
        }

        private ICommand ProcessCommand(string commandLine)
        {
            string commandName = this.GetCommand(commandLine);
            List<string> parameters = this.GetParameters(commandLine);

            ICommand command = this.commandFactory.CreateCommand(commandName, parameters, this.productRepository);
            return command;
        }

        private string GetCommand(string commandLine)
        {
            string commandName = commandLine.Split(splitCharacters, StringSplitOptions.RemoveEmptyEntries)[0];
            return commandName;
        }

        private List<string> GetParameters(string commandLine)
        {
            string[] commandParts = commandLine.Split(splitCharacters, StringSplitOptions.RemoveEmptyEntries);
            List<string> parameters = new List<string>();
            for (int i = 1; i < commandParts.Length; i++)
            {
                parameters.Add(commandParts[i]);
            }
            return parameters;
        }
    }
}