# Udemy - Demo

## Pattern how to write input for different commands.

- createCourse name, price, courseType
- createLecture name
- addLectureToCourse lectureId, courseId
- removeLectureFromCourse lectureId, courseId
- findLectureByName name
- findCourseByName name
- printAllCourses
- printAllLectures

## Steps to implement commands:

### CreateCourse:
- Validate parameters count parse paramters then pass them to constuctor.
Return CommandMessages.Successfully_Created

### CreateLecture:
- Validate parameters count parse paramters then pass them to constuctor.

### AddLectureToCourse:
- Validate parameters find course by Id and add lecture to it.

### RemoveLectureFromCourse:
- Validate parameters find course by Id and remove lecture from it.

### FindByName:
- Validate parameters count and find lecture/course by name

### PrintAll:
- Take collection from repository and collection them in stringbuilder while using ToString method from the current unit.
