﻿namespace Udemy.Shared.Enums
{
    public enum CourseType : byte
    {
        CSharp = 0,
        SQL = 2
    }
}