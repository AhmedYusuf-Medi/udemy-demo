﻿namespace Udemy.Shared.Extensions
{
    using System;
    using Udemy.Shared.Constants;

    public static class NumberExtensions
    {
        public static void ShouldBeInRange(this int value, int min, int max)
        {
            if (value < min || value > max)
            {
                throw new ArgumentException(string.Format(ExceptionMessages.Number_Must_Be_In_Range, nameof(value), min, max));
            }
        }

        public static void ShouldBeInRange(this decimal value, int min, decimal max)
        {
            if (value < min || value > max)
            {
                throw new ArgumentException(string.Format(ExceptionMessages.Number_Must_Be_In_Range, nameof(value), min, max));
            }
        }

        public static int TryParse(this string value)
        {
            if (!int.TryParse(value, out int parsedValue))
            {
                throw new ArgumentException(ExceptionMessages.Must_Be_Integer);
            }

            return parsedValue;
        }
    }
}