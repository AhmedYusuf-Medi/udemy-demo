﻿namespace Udemy.Shared.Extensions
{
    using System;

    public static class BaseValidator
    {
        public static T ThrowIfNull<T>(this T data)
        {
            if (data is null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            return data;
        }
    }
}