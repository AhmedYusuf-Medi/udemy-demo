﻿namespace Udemy.Shared.Extensions
{
    using System;
    using Udemy.Shared.Constants;
    using Udemy.Shared.Enums;

    public static class EnumExtensions
    {
        public static CourseType TryParse(this string value)
        {
            if (!Enum.TryParse(value, true, out CourseType parsedValue))
            {
                throw new ArgumentException(ExceptionMessages.Must_Be_CourseType);
            }

            return parsedValue;
        }
    }
}