﻿namespace Udemy.Shared.Extensions
{
    using System;
    using Udemy.Shared.Constants;

    public static class StringExtensions
    {
        public static void ThrowIfNullOrWhiteSpace(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException(string.Format(ExceptionMessages.String_Cannot_Be_Null_Or_WhiteSpace, nameof(value)));
            }
        }

        public static void ThrowIfOutOfRange(this string value, int minLength, int maxLength)
        {
            if (value.Length < minLength || value.Length > maxLength)
            {
                throw new ArgumentException(string.Format(ExceptionMessages.String_Must_Be_In_Range, nameof(value), minLength, maxLength));
            }
        }
    }
}