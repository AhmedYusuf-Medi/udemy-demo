﻿namespace Udemy.Shared.Constants
{
    public static class ExceptionMessages
    {
        public const string String_Cannot_Be_Null_Or_WhiteSpace = "{0} cannot be null or white space!";
        public const string String_Must_Be_In_Range = "{0} must be between {1} and {2} chars!";
        public const string Number_Must_Be_In_Range = "{0} must be between {1} and {2}!";
        public const string Must_Be_Integer = "Must be of integer type!";
        public const string Must_Be_CourseType = "Must be valid course type!";
        public const string Already_Registered = "{0} is already registered!";
    }
}