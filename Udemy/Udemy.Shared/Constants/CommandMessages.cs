﻿namespace Udemy.Shared.Constants
{
    public static class CommandMessages
    {
        public const string Successfully_Added = "Succesfully added {0} to {1}!";
        public const string Successfully_Removed = "Succesfully removed {0} from {1}!";

        /// <summary>
        /// In the holder place the newInstance.ToString();
        /// Example:
        /// var lecture = this.repository.CreateLecture(name);
        /// string.Format(CommandMessages.SuccessfullyCreated, lecture.ToString());   
        /// </summary>
        public const string Successfully_Created = "Succesfully created {0}!";
        /// <summary>
        /// In the holders place lecture Id and course Id
        /// </summary>
        public const string Successfully_Added_ToCourse = "Successfully added Lecture with Id:{0} to Course with Id:{1}!";
    }
}