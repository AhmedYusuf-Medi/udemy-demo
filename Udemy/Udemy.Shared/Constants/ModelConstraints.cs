﻿namespace Udemy.Shared.Constants
{
    public static class ModelConstraints
    {
        public const int Min_Lecture_Name = 3;
        public const int Max_Lecture_Name = 25;
        public const int Min_Course_Name = 3;
        public const int Max_Course_Name = 20;
    }
}